---
date:   2017-05-17 12:45:00 +0100
author: "Simon Blas"
tags: ci
---

 Compte rendu du 31 mai 2017

# Dernier compte rendu de l'année!

# Pas de tuto d'Hugo

# Propositions pour l'année prochaine

Organiser des mini compétitions internes aux clubs, ça pourrait être fun.
Reprendre les tutos programmation.
Continuer les tutos d'Hugo.
Organiser un roulement entre les différents membres du club qui font les tutos.
Faire un tuto git dès le début de l'année, et un bon, parce que Git c'est vraiment important.
Faire plus d'activités en groupe, pas que des réunions, par exemple des "soirées club info".

Si vous avez d'autres suggestions, n'hésitez pas dans les commentaires.

Voila voila, fin de la réunion. Bon courage pour vos ds, et bonnes vacances ensuite!

