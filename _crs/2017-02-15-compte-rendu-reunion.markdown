---
date:   2017-02-15 13:45:00 +0100
author: "Geoffrey Preud'homme & Florian Deze"
tags: ci
---

# Site internet du club
Le sachiez-vous ? On a un site internet, <https://clubinfo.plil.net>, et Geoffrey avait codé ça il y a deux ans avec des technologies cools pour l'époque mais aujourd'hui le code ressemble plutôt à des spaghettis. Et il y a plein de fonctionalités inutiles qu'on utilise pas, du genre un forum, une liste de membres, un bon pour un câlin gratuit... L'idée serait d'étendre [le sous-site de compte rendus](https://clubinfo.plil.net/cr) qui pour le coup est plutôt propre (la source est [ici](https://clubinfo.plil.net)) pour en faire le site principal, ainsi qu'éventuellement rajouter du contenu. Pour cela, Justine, Nicolas, Hugo, Éloi et Geoffrey se caleront une heure ou deux pour voir comment faire et le faire.

# Projet Etunicorn
La liste BDE élue renégocie le contrat avec Pumpkin demain, et d'après ce que j'ai compris ils ne le renouveleront que si Pumpkin leur permet de faire à peu près la même chose qu'Etunicorn. À suivre donc...

# Point vêtement

Deux fournisseurs :

## S.O.S. T-Shirts
Polo : 21 €
Sweat : 30 €
Broderie : +15 € / pièce donc on oublie

## ID Shirts
Polo : 19,80 €
Sweat : 27 €
Logo Polytech sur la manche droite : +2 €
(les interessés ont émis le choix sur si on le mettait ou pas)

Donc on part sur ID Shirts car moins cher, notamment grâce à la promo pour les gens de Polytech. Justine commandera d'ici les vacances, vous avez le temps de modifier le sondage d'ici là mais après ça sera trop tard.

# Site internet du Fabricarium

Romain le fait avec Django, si des gens sont interessés pour voir le code (il fera un effort de documentation), il sera sur <https://archives.plil.net>

# Tutoriel JSON

Le JSON est un moyen standardisé et simple d'envoyer des données via le réseau (par exemple, entre le client de Kilobits et le serveur). La spécification est [ici](http://json.org), mais rien ne vaut un petit exemple pour comprendre le principe :

```json
[
    {
        "prénom": "Benoit",
        "nom": "Verhaeghe",
        "age": 21
        "estGentil": true,
        "parleFort": true,
    },
    {
        "prénom": "Florian",
        "nom": "Deze",
        "estGentil": true,
        "saitCoderEn": [
            "java",
            "html",
            "sql"
        ]
    }
]
```
(je l'ai écrit avec pas mal de retours à la ligne pour la lisibilité, mais on peut tout condenser si on veut)

# Afterwork 28 février

Avec l'EESTEC et Robotech. Nous on fera à manger des salades de pates, l'EESTEC fera le dessert et Robotech aide. Veuillez indiquer vos disponibilités provisoires pour la préparation [ici](https://framadate.org/znHFWdwavDxT6FkX).

# Vidéo de présentation

Oh là là, de sacrées idées

## Choses à faire

- Caser les projets faits et à venir
- Séquences de la Nuit de l'Info (les rushs ont été conservés)
- Montrer que c'est pas que pour les GIS et les IMA
- Faire un meilleur mixage (on a un VRAI membre du Studio parmi nous !)
- Reprendre le gag du lecteur de disque

## Si on reprend la même trame que la dernière fois

- Recycler certaines scènes
- Scène avec tous les membres du club (pour montrer qu'on est beaucoup : petit plus pour la crédibilité)
- Refaire la scène sur le fait qu'il y a pas de fille, parce que là c'est plus le cas (enfin ça l'étais déjà pas avant, mais encore moins)

## Idées en vrac

- « Tu saurais faire ça ? - *tape sur la console* - *tout Polytech s'éteint* »
- Il faut des explosions et un générique à la PULCE (voir les deux en même temps)
- « Retournez bosser ! - Ça compile ! - Ah bah continuez à jouer alors » [(référence)](https://www.xkcd.com/303/)
- ~~On a des locaux exceptionnel~~ on va se mettre le BDE à dos x)
- On est comme la SNCF, toujours en retard

## Idées de trame

- Draw my life sur paint
- Présentation en mode trailer de film

## Compétences exceptionelles de nos membres pouvant être utiles

- Simon sait dessiner des écureuils
- Maëva sait dessiner des cygnes
- Justine sait raconter des histoires

Si avec ça on ramène pas au mois 30 personnes dans notre club...
