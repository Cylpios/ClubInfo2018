---
date:   2017-03-08 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

# Passations!!!!!!!!!!!!!!!!

Et oui c'est officiel, le bureau a changé. On a donc un nouveau président, Benoit (Viva El Presidente!), un nouveau vice-président, Hugo 
(prévenez le si il n'est pas au courant), une nouvelle trésorière, Justine (et oui! encore une fille au bureau du club Info, c'est fou) 
et enfin un nouveau secrétaire, Simon (c'est moi). 

Un discours plein d'émotions à été fait par notre bien-aimé ancien président, et un discours plein de pièces du robot secondaire à été 
fait par notre bien aimé nouveau président. Les clés du club ont été officiellement transmises, les papiers signés. Un évènement très 
solennel et riche en émotions.

# Photo du club info

Elles ont été prises. Merci Eloi.

# Vidéo du club info

L'idée 'Trailer de film' a été retenue. Le 'Draw my life' a été écarté pour le moment, car personne ici ne sais dessiner.
On a beaucoup d'idées en stock, ne reste plus que la réalisation. Si vous en avez d'autre cependant, n'hésitez pas à la communiquer à Benoit, ou a notre Respo com, Nicolas.

# Le futur

Plus de membres! Mais pas trop quand même.
Plus de projets! Parce que les projets c'est cool.
Finir Kilobits. Eventuellement.
Plus de nuit de l'info! Notre président en fait son cheval de bataille.


Voila pour ce premier compte rendu. Une excellente journée/soirée/nuit, et à bientôt pour l'ordre du jour!