---
date:   2018-03-07 13:45:00 +0100
author: "Blas Simon"
tags: ci
---

Bonjour à tous et à toutes!

Voici le compte rendu du 7 mars 2018.

## Aujourd'hui:

Regarder le site du Bde. Si quelqu'un à des choses à y dire, n'hésitez pas!

## Tournois Battledev:

Tout le club Info sera inscrit par défaut.
C'est un concours, fait dans le language que vous choisirez (C, C++, C#, Java, NodeJS, Php, python, Ruby)
, l'objectif est de réussir le premier exercice. On peut le faire tous ensembles, ça sera sympa.
Vous pouvez vous y inscrire jusqu'au 17-18 mars, le tournois se passera le 27 mars à 20h00.
Pour l'instant, Polytech n'est pas listé en école participante, mais vous pouvez vous y 
inscrire de façon indépendante, ou attendre que Polytech soit ajouté à la liste.

Si vous êtes bien placés dans le tournois, vous pourrez éventuellement être contactés par des entreprises ;).

## Préparation

On organisera des séances de prép à la battledev, un soir ou un week end. On dispose des quatres exercices (merci justine) de l'année dernière.

## La Geoffre:

Suite à une perte de téléphone de notre bien aimé respo com, on a temporairement 
perdu nos photos de la Geoffre. Cependant, on a une solution, le problème sera réglé prochainement.
On doit aller voir madame Pichonat pour la Geoffre prochainement, puis lui parler de 
notre super projet.

On va essayer de se joindre à la Crep si ils font un after work, ça nous fera de la pub.

## Les tutos!!

- La semaine prochaine, un tuto Ruby de notre président!! :D
- La semaine suivante, un tuto d'Hugo!!! Enfin.

## Le semaine prochaine:

- La chasse au trésor
- La vidéo de présentation de l'année prochaine
- La nuit de l'info!

### Fin de la réunion!
Une bonne journée à tous.
