---
date:   2018-01-24 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

#### Compte rendu du 24 janvier 2018

Bonjour à tous! 

Sans transition, le compte rendu:

### Le retour de Johnny

### Les aventures de Benoit

Sex toys connectés et gestion du stress.

### Le club info dans les écoles

Première scéance demain! (jeudi 25/01/2018) Nos premiers intervenants tests seront Hugo et Nicolas.
On croit en eux pour représenter dignement le club Info :).<br>
Elle se déroulera de 13h40 à 16h15 dans une école de Gambetta, avec un groupe relativement important de 23 élèves.<br>
Le but est d'expliquer aux enfants les bases de la programmation à l'aide de Scratch, avec l'explication des variables, des boucles, des conditions et des actions.

Voila voila, fin de la réunion!

#### Une bonne journée à tous