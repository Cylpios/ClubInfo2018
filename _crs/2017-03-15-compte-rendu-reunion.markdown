---
date:   2017-03-15 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

Bonjour à toutes/tous, voila le compte rendu de la réunion du 15 mars 2017 
(au cas où vous ne sauriez pas lire un titre).

On commence par le meilleur (ou pas):

# Point surprise de geoffrey

Il a envoyé un mail au bureau du club info pour voir si il suivait mais 
personne n'a répondu, donc apparemment non. On a donc eu l'honneur d'obtenir
les infos en direct, que voici: Nous avons étés contactés par Madame Emmanuelle
Pinochat, qui voudrait qu'un ou deux élèves de Polytech faisant partie du club 
Info ou de Robotech participe à Néojobs, une journée de presentation des métiers 
de l'informatique sur Lille. Elle se déroulerait le 31 mars, et le/la 
participante devra aider Madame Pichonat à présenter les formations de Polytech,
et servir de support technique en cas de problèmes. Si vous êtes volontaires, 
prévenez Geoffrey. Ou Benoit. Ou n'importe qui qui puisse communiquer avec 
Geoffrey en fait.

# Point Justine 

Merci d'avoir nourri le club Info ce midi.
Merci d'avoir habillé le club Info ce midi.

# Point lessive

Consignes d'entretien:
Pour une conservation optimale de votre T-Shirt/Polo/Whatever:
Lavage à 40 degrès maxi, et à l'envers!
Ne pas repasser sur le transfert (ne pas hésiter à le repasser à l'envers).
Pas de sèche-linge!

Fin du point lessive.

# Point Réparation

Alexis est actuellement en train de réparer (ou pas) l'ordinateur d'une iaal.
Si il est irréparable, il en sera fait don au club info. Sinon, on lui dira 
qu'il est irréparable quand même et il en sera fait don au club info.
Dans tous les cas, attention à sauvegarder les données et à les rendre à la 
gentille Iaal qui nous fait confiance.

# Point surprise Hardware de Hugo (on était déjà au courant)

Le président se décharge de toute responsabilité.
Apparemment, faire de l'Hardware au club info, ça serait cool.
Et les gens sont pour!
On devrait donc prochainement pouvoir profiter de tutos sur le matériel
informatique, qu'y-a-t-il dans un ordinateur, etc....
On apprendra également à étaler de la pâte thermique avec une carte Bleue, même 
si ça fonctionne avec n'importe quelle carte importante.
Voila voila, donc on va pouvoir apprendre encore plus de trucs, fantastique.

# Point soirée de passations

Il y aura un afterwork puis une soirée de passations officielle le vendredi
31 mars. Surtout, n'y allez pas! Apparemment, c'est un bazar pas possible.
A la place, notre glorieux club info nous propose une soirée de passations 
tranquilou pépére avec Robotech le mercredi 29. Et la population du club info
à l'air plutot enthousiasmée à l'idée de ne pas aller à la soirée du 31. 
Donc réservez votre 29!

# Point eeeeeeeeeeeeeeeeuuuuuuuuuuuuuuuuuuuuuuh

Le président à fait eeeeeeeeeeeeeeeeeeeeeeuuuuuuuuuuuuuuuuuuuuuuuuh.

# Point kilobits

Marianne aimerait avancer mais est bloquée par la non-vérification des requêtes
SQL (on se demande bien qui devait le faire). Ca devrait être fait d'ici à la 
semaine prochaine. Nicolas à fini son index mais doit encore push, apparemment
push c'est compliqué. Justine n'a toujours pas push sa modification non plus.
Pourtant ça fait au moins deux mois qu'elle est finie. Apparemment push c'est 
vraiment compliqué.. 
En résumé: pour l'instant Kilobits à calé, mais Benoit tente de relancer le 
moteur. Y parviendra-t-il? Rendez vous la semaine prochaine pour la réponse et
pour notre série de très très très très longue durée!

# Point le plus important!!!!! ~~passations~~ La vidéo de présentation du club info

On a deux minutes de vidéo. Il a donc été décidé collégialement de réaliser une
série de ~~scetch~~ sketchs à but humoristique, et éventuellement, informatif.
Le premier sketch sera celui des fameux Hackeurs du club info, avec dans la 
réalité, un génie qui fait crash windows. Ou linux.

Le second sketch montrera le bureau du club info de manière badass sur un fond
de Far West puis le bureau du club info sous son vrai jour (à vous de voir ce 
que ça veux dire).

Le troisième sketch expliquera qu'il n'y a que des garçons aux cheveux longs au 
club info, puis le montrera sous son vrai jour! (Une majorité de garçons aux 
cheuveux longs ou pas, et un grand total de trois filles.)

Le quatrième sketch montrera la fameuse salle reservée au club info. (pas besoin
de plus d'explications)

Le cinquième sketch sera à nouveau un sketch de hacking, incluant un site appelé
Hack Poney? Benoit se débrouillera avec ça.

Le sixième sketch parlera de tous l'argent qu'a le club info, puis du talent des
gens du club info pour falsifier des comptes.

Le septième (oui il en reste encore quatre après) sketch parlera de quelqu'un 
qui essaye d'aider un individu essayant de programmer. Et ça impliquera tous
les membres. Et ça finira en "éteindre et rallumer l'ordi".

Le 8ème (oui j'en ait eu marre), parlera du président. Pas plus d'infos.

Le 9ème (plus que deux!!!) sketch parlera d'Etunicorn, et de notre capacité 
(ou pas) à nous faire de l'argent sans problèmes et sans rien faire...... 
(ou pas).

Le 10ème (ça n'en finira donc jamais?!?) sketch parlera d'une légendaire 
capacité qu'aurait justine. Celle de dire oui. Mais es-ce vraiment une simple 
légende?

Le 11ème (.......) une scène bonus, incluant le président qui trash talk le 
club info, puis le président qui devient président du club info (je parle du 
nouveau, pas de l'ancien).

On aura le bonheur d'avoir Eloi (Illoy) en tant que réalisateur. Merci Eloi.

# Teasing

Une image teaser du bureau sera révelée la semaine prochaine.
Le bureau sera révelé la semaine suivante.
Le reste du club sera révelé la semaine suivante.
Voila. Le teasing c'est important. Pour la com. Et rendre les gens curieux. Et
tout et tout.




Fin du compte rendu!! Une bonne soirée/journée/matinée/après midi à tous! :D
Retrouvez nous la semaine prochaine, pour un prochain épisode de compte rendus
du club info!!!! Par moi.