---
date:   2017-09-20 13:45:00 +0100
author: "Simon Blas"
tags: ci
---

# Compte rendu du mercredi 20 septembre 2017

Au programme aujourd'hui:
- Présentation du(es) nouveau(x)
- Utiliser Trello
- Pas de tutos d'Hugo T-T
- Organiser l'équipe pour le site web du Bde
- Présentation de la nuit de l'info
- Rapport sur le repas
- Ajouter le(s) nouveau(x) aux différents groupes

On a deux nouveaux! Ils s'appellent Clément et Robin, soyez gentils avec eux.
Soyez les bienvenus ;).

Hugo présent aujourd'hui mais pas de tutos d'Hugo. Tristesse. Le programme des tutos:
- semaine prochaine: Benoit
- dans deux semaines: Geoffrey
- dans trois semaines: Hugo

Organiser l'équipe du site web Bde:
- Le Bde nous a demandé de réaliser un site web et on a carte blanche pour le faire. Le club Info, il a des projets cools.
Sont volontaires: Nicolas et Benoit, plus Robin, Eloi fournira les photos.
Ils vont nous sortir une maquette pour dans deux semaines, et on pourra la juger ensuite. Mouahahah.

Le repas:
- On a dépensé 35.73€, et au total on a réalisé 16€ et trois carottes de bénefices.
Bravo tout le monde.

Présentation Nuit de l'Info:
- Ca se passe du 7 au 8 décembre! C'est une nuit passée à programmer sur un sujet en équipe.
On va faire une équipe club Info, et on va commencer à s'organiser dès la semaine prochaine.
Pas besoin de compétences particulières, on vous fera des tutoriels au fur et à mesure
pour mettre tout le monde à niveau. Il y a des concours proposés par des entreprises,
auxquels on peut participer et qui peuvent nous rapporter des prix. L'année dernière, 
troisième meilleure vidéo de France, on était plutôt heureux ^^.

Fin de la réunion, une bonne journée à tous! :D
