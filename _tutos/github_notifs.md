---
title: "Désactiver les notifications GitHub"
tags: github ci
author: "Geoffrey Preud'homme"
date: 2015-04-15 15:05:15
---

Par défaut, à chaque fois qu'un nouveau dépôt   `git` est crée sur GitHub, tous les membres du Club Info sont ajouté comme contributeur.
Cela permet de ne pas s'embêter avec les droits, et ainsi tout le monde peut contribuer, même s'il n'avait pas émit le souhait de contribuer au préalable !

Par contre, pour ceux qui ne souhaitent pas s'interesser au projet, ces derniers puevent reçoivent quand même des notifications GitHub, qui sont relayées par mail. Il y a deux façons de palier à cela.

* Ne recevoir de mail pour aucun dépôt GitHub
*(méthode radicale, vous continuerez à recevoir des notifications web sur le site de GitHub)*
Il suffit d'aller dans les [paramètres de notification GitHub](https://github.com/settings/notifications), et de décocher <input type="checkbox"/> **Email** sous la rangée **Watching**.

* Se désabonner des projets que l'on ne veut pas suivre
*(permet de garder certains dépôt, et supprimer tous les types de notifications pour les autres)*
Pour les dépôts que vous ne voulez pas suivre, allez sur leur page (exemple : <https://github.com/ClubInfoPolytechLille/ci-site>), puis cliquez sur **(Un)Watch** en haut à droite et choisissez **Not watching**.

Vous pouvez aussi **ne pas suivre automatiquement les nouveaux projets**, pour cela, rendez-vous dans les [paramètres de notification GitHub](https://github.com/settings/notifications) et décochez **Automatically watch repositories**.

Voilà !

